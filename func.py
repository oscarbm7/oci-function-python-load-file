# Copyright (c) 2016, 2018, Oracle and/or its affiliates.  All rights reserved.
import io
import os
import json
import sys
import logging
from fdk import response
from object_storage import move_object
from adb import get_connection, run_procedure

import oci.object_storage

def handler(ctx, data: io.BytesIO = None):
    logging.getLogger().info('signer request')
    signer = oci.auth.signers.get_resource_principals_signer()
    
    try:        
        body = json.loads(data.getvalue())
        logging.getLogger().info('request body: {0}'.format(str(body)))        
    except Exception as e:
        error = 'Input a JSON object in the format: ' + e
        logging.getLogger().error('ERROR: ' + error)
        raise Exception(error)

    resp = do(signer, body)

    return response.Response(
        ctx, response_data=json.dumps(resp),
        headers={"Content-Type": "application/json"}
    )

def do(signer, body):
    try:
        object_name = body["data"]["resourceName"]
        namespace = body["data"]["additionalDetails"]["namespace"]
        bucket_name = body["data"]["additionalDetails"]["bucketName"]

        if object_name.lower().find("csv") > 0:
            new_object_name = move_object(signer, object_name, 'ProcessedFiles', namespace, bucket_name)
            logging.getLogger().info("rename object Done")

            dbwallet_dir = os.getenv('TNS_ADMIN')
            dbconnection = get_connection(dbwallet_dir)
            logging.getLogger().info("Successfully retrieved connection")

            run_procedure('CREATE_LOAD_FILE_JOB',dbconnection, new_object_name, namespace, bucket_name)
 
        else:
            raise SystemExit("File extension is not supported")    
            
    except Exception as e:
        logging.getLogger().error("Failed:" + str(e))
        raise SystemExit(str(e))
        
    response = {
        "content": "OK"
    }
    return response