import os
import logging
import oci
import uuid

def move_object(signer, object_name, dir_dest_name, namespace, bucket_name):

    event_id = uuid.uuid4().hex
    new_object_name = dir_dest_name + "/" + object_name.replace(".csv", event_id + ".csv")
    new_object_name = new_object_name.replace(" ", "")
    logging.getLogger().info("eventID: "+str(event_id))
    try: 
        object_storage_client = oci.object_storage.ObjectStorageClient(config={}, signer=signer)

        resp = object_storage_client.rename_object(
        namespace_name=namespace,
        bucket_name=bucket_name,
        rename_object_details=oci.object_storage.models.RenameObjectDetails(
            source_name=object_name,
            new_name=new_object_name))

        if resp.data.status != "COMPLETED":
            raise Exception("cannot rename object {0}".format(object_name))
        else:
            logging.getLogger().info("INFO - Object {0} moved to Folder {1}".format(object_name,dir_dest_name))
    except Exception as e:
            logging.getLogger().error('ERROR Rename:' + str(e))

    return new_object_name
