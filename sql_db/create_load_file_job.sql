create or replace PROCEDURE CREATE_LOAD_FILE_JOB ( FILE_NAME IN VARCHAR2, 
                                                   NAMESPACE IN VARCHAR2,
                                                   BUCKET IN VARCHAR2,
                                                   OUTPUT OUT VARCHAR ) AS

    JOB_ID NUMBER(10);  
    JOB_NAME VARCHAR2(100);

BEGIN
    
    Select TRUNC(DBMS_RANDOM.value(low => 1, high => 65535)) 
    Into JOB_ID
    From dual;
    
    JOB_NAME := 'UPLOAD_'||JOB_ID;
    
    DBMS_SCHEDULER.create_job (
    job_name => JOB_NAME,
    job_type => 'PLSQL_BLOCK',
    job_action  => 'BEGIN LOAD_FILE(FILE_NAME => '''||FILE_NAME||''', JOB_NAME=> '''||JOB_NAME||''', NAMESPACE=> '''||NAMESPACE||''', BUCKET=> '''||BUCKET||'''); END;',
    start_date => SYSTIMESTAMP,
    end_date => NULL,
    enabled => TRUE, 
    auto_drop => TRUE,
    comments => 'Load Consumption File: '||FILE_NAME);
    
    OUTPUT := 'Creation '||JOB_NAME||' Done';

    
EXCEPTION 
   WHEN OTHERS THEN 
   dbms_output.put_line(SQLERRM ||'-'|| dbms_utility.format_error_backtrace);
   OUTPUT := SQLERRM ||'-'|| dbms_utility.format_error_backtrace;
END CREATE_LOAD_FILE_JOB;

/
