create or replace PROCEDURE LOAD_FILE ( FILE_NAME IN VARCHAR2, 
                                        JOB_NAME IN VARCHAR2, 
                                        NAMESPACE IN VARCHAR2,
                                        BUCKET IN VARCHAR2) AS

  L_RESP DBMS_CLOUD_TYPES.resp;
  l_OPERATION_ID      NUMBER;
  l_FIELD_LIST        CLOB :=
    '"COMPUTE_TYPE"          CHAR(100)
    ,"DATA_CENTER"           CHAR(50)
    ,"METERED_SERVICE_DATE"  CHAR date_format DATE MASK "MM-DD-YYYY"
    ,"COMPUTED_QUANTITY"     CHAR
    ,"USAGE_QUANTITY"        CHAR
    ,"UOM"                   CHAR(1000)
    ,"PRODUCT"               CHAR(1000)
    ,"NET_UNIT_PRICE"        CHAR
    ,"LINE_NET_AMOUNT"       CHAR
    ,"OVERAGE"               CHAR(20)
    ,"STATUS"                CHAR(50)
    ,"AVALIABLE_AMT_AFTER_COMPUTING"      CHAR    
    ,"SERVICE_TO_CUSTOMER"      CHAR(500)
    ,"TYPE"                  CHAR(20)';
    
  l_FORMAT            CLOB :=
    '{
       "delimiter" : ",",
       "ignoremissingcolumns" : true,
       "ignoreblanklines" : true,
       "blankasnull" : true,
       "trimspaces" : "lrtrim",
       "quote" : "\"",
       "characterset" : "AL32UTF8",
       "skipheaders" : 1,
       "logprefix" : "CONSUMPTION_SPM",
       "rejectlimit" : 10000000,
       "recorddelimiter" : "X''0A''"
     }';
    
BEGIN

   DBMS_CLOUD.COPY_DATA
  ( TABLE_NAME        => 'CONSUMPTION_SPM'
   ,CREDENTIAL_NAME   => 'CONSUMPTION_BUCKET'
   ,FILE_URI_LIST     => 'https://objectstorage.us-ashburn-1.oraclecloud.com/n/'||NAMESPACE||'/b/'||BUCKET||'/o/'||FILE_NAME
   ,FIELD_LIST        => l_FIELD_LIST
   ,FORMAT            => l_FORMAT
   ,OPERATION_ID      => l_OPERATION_ID
  );


  commit; 

END LOAD_FILE;

/
