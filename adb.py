import io
import oci
import os
import random
import oracledb
import logging
from zipfile import ZipFile
from timeit import default_timer as timer
import string


def get_dbwallet_from_autonomousdb(dbwallet_dir):
    dbwalletzip_location = "/tmp/dbwallet.zip"
    adb_ocid = os.getenv("ADB_OCID")     

    signer = oci.auth.signers.get_resource_principals_signer()   # authentication based on instance principal
    atp_client = oci.database.DatabaseClient(config={}, signer=signer)
    atp_wallet_pwd = ''.join(random.choices(string.ascii_uppercase + string.digits, k=15)) # random string
    # the wallet password is only used for creation of the Java jks files, which aren't used by cx_Oracle so the value is not important
    atp_wallet_details = oci.database.models.GenerateAutonomousDatabaseWalletDetails(password=atp_wallet_pwd)
    print(atp_wallet_details, flush=True)
    obj = atp_client.generate_autonomous_database_wallet(adb_ocid, atp_wallet_details)
    with open(dbwalletzip_location, 'w+b') as f:
        for chunk in obj.data.raw.stream(1024 * 1024, decode_content=False):
            f.write(chunk)
    with ZipFile(dbwalletzip_location, 'r') as zipObj:
            zipObj.extractall(dbwallet_dir)
    logging.getLogger().info("wallet generated.......")     
    return atp_wallet_pwd   


def get_connection(dbwallet_dir):          
    dbuser = os.getenv("DBUSER")
    dbpwd = os.getenv("DBPWD")
    dbsvc = os.getenv("DBSVC")
    wallet_password = get_dbwallet_from_autonomousdb(dbwallet_dir)

    # Update SQLNET.ORA
    with open(dbwallet_dir + '/sqlnet.ora') as orig_sqlnetora:
        newText=orig_sqlnetora.read().replace('DIRECTORY=\"?/network/admin\"', 
        'DIRECTORY=\"{}\"'.format(dbwallet_dir))
    with open(dbwallet_dir + '/sqlnet.ora', "w") as new_sqlnetora:
        new_sqlnetora.write(newText)

    logging.getLogger().info("sqlnet.ora: "+ newText )
    # Create the DB Session Pool            
    logging.getLogger().info("dbwallet_dir: "+ dbwallet_dir )
    dbconnection = oracledb.connect(user=dbuser, password=dbpwd, dsn=dbsvc,
                              config_dir=dbwallet_dir, wallet_location=dbwallet_dir, 
                              wallet_password=wallet_password)    

    logging.getLogger().info("Connection "+ dbsvc +" created ")
    return dbconnection


def run_procedure(procedure_statement, dbconnection, object_name, namespace, bucket_name):    
    with dbconnection.cursor() as dbcursor:
        start_query = timer()
        out_val = dbcursor.var(str)
        dbcursor.callproc(procedure_statement, [object_name, namespace, bucket_name, out_val])
        end_query = timer()  
        logging.getLogger().info("Outcome procedure " + out_val.getvalue())
        logging.getLogger().info("INFO: DB procedure executed in {} sec".format(end_query - start_query))